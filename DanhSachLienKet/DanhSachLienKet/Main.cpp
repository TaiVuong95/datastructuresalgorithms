#include <iostream>
#include <conio.h>
//Khai bao kieu du lieu Node
struct node
{
	int info;
	struct node * pNext;
};
typedef struct node NODE;
//Khai bao kieu du lieu List
struct list
{
	NODE *pHead;
	NODE *pTail;
};
typedef struct list LIST;

//Ham khoi tao List
void init(LIST &l){
	l.pHead=NULL;
	l.pTail=NULL;
}

//Tao 1 Node moi
NODE* GetNode(int x){
	NODE*p= new NODE;
	if(p==NULL){
		return NULL;
	}
	p->info=x;
	p->pNext=NULL;
	return p;

}

//Them 1 node vao dau link list

void AddHead(LIST &l,NODE *p){
	if(l.pHead==NULL){
		l.pHead=p;
		l.pTail=p;
	}
	else{
		p->pNext=l.pHead;
		l.pHead=p;

	}
}


//Them 1 node vao cuoi link list
void AddTail(LIST &l,NODE *p){
	if(l.pHead==NULL){
		l.pHead=p;
		l.pTail=p;
	}
	else{
		l.pTail->pNext=p;
		l.pTail=p;

	}
}
//Ham nhap
void Input(LIST &l){
	int n;
	printf("Moi ban nhap vao so N :");
	scanf("%d",&n);
	init(l);
	for(int i=1;i<=n;i++){
		int x;
		printf("Moi ban nhap vao x:");
		scanf("%d",&x);
		NODE *p = GetNode(x);
		if(p!=NULL){
			//AddHead(l,p);
			AddTail(l,p);
		}
	}

}

//Ham xuat ra cac phan tu
void Output(LIST l){
	NODE*p=l.pHead;
	while(p!=NULL){
		printf("%d ",p->info);
		p=p->pNext;
	}
}

//Ham tinh tong
int TinhTong(LIST l){
	NODE*p=l.pHead;
	int sum=0;
	while(p!=NULL){
		sum=sum+p->info;
		p=p->pNext;
	}
	return sum;
}

int main(){
	LIST lst;
	Input(lst);
	Output(lst);
	printf("\n");
	printf("Tong la : %d",TinhTong(lst));
	getch();
	return 0;
}