#ifndef _THEM_VAO_H
#define _THEM_VAO_H
#include "KhaiBao.h"

namespace std{
int insertNode(Tree &T, item x) // Tham bien con trokj
{
    if (T != NULL)
    {
        if (T->key == x) return -1;  // Node nay da co
        if (T->key > x) return insertNode(T->Left, x); // chen vao Node trai
        else if (T->key < x) return insertNode(T->Right, x);   // chen vao Node phai
    }
    T = (Node*) malloc(sizeof(Node));
	if (T == NULL) {return 0;}    // khong du bo nho
    T->key = x;
    T->Left = T->Right = NULL;
    return 1;   // ok
}
}
#endif