#ifndef _COUNT_H
#define _COUNT_H
#include "KhaiBao.h"
#include <iostream>

namespace std{

int count(Tree T)
{
	int a=0,b=0;
	Tree TT=T;
	while(T!=NULL){
		++a;
		T=T->Left;
	}
	while(TT!=NULL){
		++b;
		TT=TT->Right;
	}
	if(a>b){return a;}
	else{return b;}

}
}
#endif