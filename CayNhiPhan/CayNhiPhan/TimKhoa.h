#ifndef _TIM_KHOA_H
#define _TIM_KHOA_H
#include "KhaiBao.h"


namespace std{
Node* searchKey(Tree T, item x)     // tim nut co key x
{
    if (T!=NULL)
    {
        if (T->key == x) { Node *P = T; return P;}
        if (T->key > x) return searchKey(T->Left, x);
        if (T->key < x) return searchKey(T->Right, x);
    }
    return NULL;
}
}
#endif